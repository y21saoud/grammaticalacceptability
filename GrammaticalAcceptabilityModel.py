import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score, accuracy_score

class GrammaticalAcceptabilityModel:
    def __init__(self, sample_size=1, vectorizer = CountVectorizer()):
        self.sample_size = sample_size
        self.model = None
        self.vectorizer = vectorizer

    def sample(self, df):
        if self.sample_size!=1:
            sampled_df = df.sample(n=int(len(df) * self.sample_size), random_state=42)
            print("sampled_df\n", sampled_df)
            return sampled_df
        return df
    
    def _tokenize(self, sentence):
        raise NotImplementedError("Subclasses must implement _tokenize method")
    
    def _vectorize(self, sentences):
        raise NotImplementedError("Subclasses must implement _vectorize method")
    
    def train(self, sentences, labels):
        raise NotImplementedError("Subclasses must implement train method")
    
    def classify(self, sentences):
        raise NotImplementedError("Subclasses must implement classify method")
    
    def evaluate(self, X_test, y_test):
        print("\nEvaluating ...")
        # Transform the test data using the fitted vectorizer
        X_test_vectors = self.vectorizer.transform(X_test["Sentence"])
        predictions = self.model.predict(X_test_vectors)

        # Assuming predictions is a numpy array
        predictions_df = pd.DataFrame(predictions, columns=['Predictions'])

        # Concatenate X_test, y_test, and predictions_df
        df_predictions = pd.concat([X_test, y_test, predictions_df], axis=1)

        # Collect bad predictions
        bad_predictions = df_predictions[df_predictions['Label'] != df_predictions['Predictions']]
        print("bad predictions :\n", bad_predictions)

        # Calculate evaluation metrics
        accuracy = accuracy_score(y_test, predictions)
        recall = recall_score(y_test, predictions, pos_label=0) 
        precision = precision_score(y_test, predictions, pos_label=0) 
        f1 = f1_score(y_test, predictions, pos_label=0) 
        confusion_mat = confusion_matrix(y_test, predictions)

        return {
            "accuracy": accuracy,
            "recall": recall,
            "precision": precision,
            "f1_score": f1,
            "confusion_matrix": confusion_mat,
            "bad_predictions": bad_predictions
        }