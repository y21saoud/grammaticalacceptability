import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.utils import resample

from model_1 import MLModel
from model_2 import MLModelNGram
from model_3 import MLModelPosTag
from model_4 import MLModelSentenceRecomposition


# Define an empty list to store the data
data = []

# Open the CSV file and read it line by line
with open("Input/data.csv", mode="r", encoding="latin1") as file:
    for line in file:
        # Split each line by semicolon
        parts = line.strip().split(';')
        if len(parts) == 2:  # Ensure there are two parts
            if parts[1] == "1":
                parts[1] = 1
            else: 
                parts[1] = 0
            data.append(parts)

# Convert the data into a DataFrame
df = pd.DataFrame(data, columns=['Sentence', 'Label'])

# Display the DataFrame
print("\ndf\n", df)

# Separate the sampled data into training and test sets
X = df["Sentence"]
y = df["Label"]
X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8, random_state=42, stratify=y)

    # Oversample only the minority class in the training set
df_train = pd.concat([X_train, y_train], axis=1)
df_majority = df_train[df_train["Label"] == 1]
df_minority = df_train[df_train["Label"] == 0]
df_minority_upsampled = resample(df_minority, replace=True, n_samples=len(df_majority), random_state=42)
df_train_upsampled = pd.concat([df_majority, df_minority_upsampled])

    # Separate the oversampled data into X_train and y_train
X_train_upsampled = df_train_upsampled["Sentence"]
y_train_upsampled = df_train_upsampled["Label"]

    # Convert X_train and X_test back to DataFrames
X_train = pd.DataFrame(X_train_upsampled, columns=['Sentence']).reset_index(drop=True)
X_test = pd.DataFrame(X_test, columns=['Sentence']).reset_index(drop=True)
y_train = pd.DataFrame(y_train_upsampled, columns=['Label']).reset_index(drop=True)
y_test = pd.DataFrame(y_test, columns=['Label']).reset_index(drop=True)

for classifier in ["RandomForest", "LogisticRegression", "XGBoost", "SVM"]:
    model = MLModelSentenceRecomposition(classifier=classifier)

    # Train the model using the oversampled training data
    model.train(X_train, y_train)

    # Evaluate the model on the original training data
    print("\nTrain evaluation ")
    df_train_evaluation = model.evaluate(X_train, y_train)
    print(df_train_evaluation)

    # Evaluate the model on the test data
    evaluation = model.evaluate(X_test, y_test)
    print("classifier: ", classifier, evaluation)

    # Plot confusion matrix
    plt.figure(figsize=(8, 6))
    sns.heatmap(evaluation["confusion_matrix"], annot=True, fmt='d', cmap='Blues', cbar=False)
    plt.title('Confusion Matrix '+classifier)
    plt.xlabel('Predicted Label')
    plt.ylabel('True Label')
    plt.show()
