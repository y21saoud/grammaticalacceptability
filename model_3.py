import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.feature_extraction.text import CountVectorizer
import xgboost as xgb
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from GrammaticalAcceptabilityModel import GrammaticalAcceptabilityModel
import spacy
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score, confusion_matrix

# Load SpaCy English model
nlp = spacy.load("en_core_web_sm")

class MLModelPosTag(GrammaticalAcceptabilityModel):
    def __init__(self, classifier='LogisticRegression'):
        super().__init__()
        self.vectorizer = CountVectorizer()
        if classifier == 'LogisticRegression':
            self.model = LogisticRegression()
        elif classifier == 'XGBoost':
            self.model = xgb.XGBClassifier()
        elif classifier == 'SVM':
            self.model = SVC()
        elif classifier == 'RandomForest':
            self.model = RandomForestClassifier()
        else:
            raise ValueError("Invalid classifier specified.")
    
    def _tokenize(self, sentence):
        doc = nlp(sentence)
        return [(token.text, token.pos_) for token in doc]

    def _vectorize(self, sentences):
        if self.vectorizer is None:
            self.vectorizer = CountVectorizer()
            X = self.vectorizer.fit_transform(sentences)
        else:
            X = self.vectorizer.transform(sentences)
        return X.toarray()
    
    def train(self, X_train, y_train):
        print("\nTraining ...")

        # Tokenize and vectorize the sentences
        X_train_tags = [self._tokenize(sentence) for sentence in X_train["Sentence"]]
        print("X_train_tags ", X_train_tags[:10])
        X_train_tags_str = [" ".join([token[1] for token in sentence]) for sentence in X_train_tags]
        X_train_vectors = self.vectorizer.fit_transform(X_train_tags_str)

        self.model.fit(X_train_vectors, y_train)
    
    def classify(self, sentence):
        # Tokenize and vectorize the input sentence
        tokens = self._tokenize(sentence)
        sentence_str = " ".join([f"{token[0]}_{token[1]}" for token in tokens])
        vector = self.vectorizer.transform([sentence_str])

        predictions = self.model.predict(vector)
        return predictions
    
    def evaluate(self, X_test, y_test):
        print("\nEvaluating ...")

        # Tokenize and vectorize the test sentences
        X_test_tags = [self._tokenize(sentence) for sentence in X_test["Sentence"]]
        X_test_tags_str = [" ".join([token[1] for token in sentence]) for sentence in X_test_tags]
        X_test_vectors = self.vectorizer.transform(X_test_tags_str)

        predictions = self.model.predict(X_test_vectors)

        predictions_df = pd.DataFrame(predictions, columns=['Predictions'])

        # Concatenate X_test, y_test, and predictions_df
        df_predictions = pd.concat([X_test, y_test, predictions_df], axis=1)

        # Collect bad predictions
        X_test_tags_str_df = pd.DataFrame(X_test_tags_str, columns=['Sentence'])
        bad_predictions = df_predictions[df_predictions['Label'] != df_predictions['Predictions']]
        bad_predictions_withtags = pd.merge(bad_predictions, X_test_tags_str_df, left_index=True, right_index=True, how='left', suffixes=('', '_tags'))
        print("bad predictions :\n", bad_predictions_withtags)

        # Calculate evaluation metrics
        accuracy = accuracy_score(y_test, predictions)
        recall = recall_score(y_test, predictions, pos_label=0) 
        precision = precision_score(y_test, predictions, pos_label=0) 
        f1 = f1_score(y_test, predictions, pos_label=0) 
        confusion_mat = confusion_matrix(y_test, predictions)
        
        return {
            "accuracy": accuracy,
            "recall": recall,
            "precision": precision,
            "f1_score": f1,
            "confusion_matrix": confusion_mat
        }
