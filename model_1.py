import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.feature_extraction.text import CountVectorizer
import nltk
from nltk.tokenize import word_tokenize
import xgboost as xgb
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier


from GrammaticalAcceptabilityModel import GrammaticalAcceptabilityModel

class MLModel(GrammaticalAcceptabilityModel):
    def __init__(self, classifier='LogisticRegression'):
        super().__init__()
        self.vectorizer = CountVectorizer()
        if classifier == 'LogisticRegression':
            self.model = LogisticRegression()
        elif classifier == 'XGBoost':
            self.model = xgb.XGBClassifier()
        elif classifier == 'SVM':
            self.model = SVC()
        elif classifier == 'RandomForest':
            self.model = RandomForestClassifier()
        else:
            raise ValueError("Invalid classifier specified.")
    
    def _tokenize(self, sentence):
        return word_tokenize(sentence)
    
    def _vectorize(self, sentences):
        if self.vectorizer is None:
            self.vectorizer = CountVectorizer()
            X = self.vectorizer.fit_transform(sentences)
        else:
            X = self.vectorizer.transform(sentences)
        return X.toarray()
    
    def train(self, X_train, y_train):
        print("\nTraining ...")
        X_train_vectors = self.vectorizer.fit_transform(X_train["Sentence"])
        self.model.fit(X_train_vectors, y_train)
    
    def classify(self, sentence):
        vector = self.vectorizer.transform([sentence])
        predictions = self.model.predict(vector)
        return predictions