import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import pandas as pd
import nltk
from nltk.tokenize import word_tokenize
from nltk.probability import FreqDist
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
from sklearn.feature_extraction.text import CountVectorizer
import matplotlib.pyplot as plt

data = []

# Open the CSV file and read it line by line
with open("Input/data.csv", mode="r", encoding="latin1") as file:
    for line in file:
        # Split each line by semicolon
        parts = line.strip().split(';')
        if len(parts) == 2:  # Ensure there are two parts
            if parts[1] == "1":
                parts[1] = 1
            else: 
                parts[1] = 0
            data.append(parts)

# Convert the data into a DataFrame
df = pd.DataFrame(data, columns=['Sentence', 'Label'])

# Display the DataFrame
print("\ndf\n", df)

print("\nValue Counts: \n",df["Label"].value_counts())

print("\ndf errors: \n", df[df["Label"]==0])

#Since the classes are imballanced, it is important to use stratify in the splitting
#The accuracy metric could also give some biased results because of that

# 1. Sentence Length Analysis
df['Sentence_Length'] = df['Sentence'].apply(lambda x: len(word_tokenize(x)))
plt.hist(df['Sentence_Length'], bins=20, color='skyblue')
plt.xlabel('Sentence Length')
plt.ylabel('Frequency')
plt.title('Distribution of Sentence Lengths')
plt.show()

# 2.1. Word Frequency Analysis
all_words = ' '.join(df['Sentence']).lower()
tokens = word_tokenize(all_words)
words_except_stop_dist = FreqDist(w for w in tokens if w.isalnum() and w.lower() not in stopwords.words('english'))
words_except_stop_dist.plot(30, cumulative=False, title='Top 30 Most Frequent Words (excluding stopwords)')

# 2.2 Word Frequency Analysis with Lemmatization
lemmatizer = WordNetLemmatizer()
racines = [lemmatizer.lemmatize(t) for t in tokens if t.lower() not in stopwords.words('english')] 
words_except_stop_dist = FreqDist(w for w in racines if w.isalnum())
words_except_stop_dist.plot(30, cumulative=False, title='Top 30 Most Frequent Words (with Lemmatization)')

# 3. Label Distribution
label_counts = df['Label'].value_counts()
plt.bar(label_counts.index.astype(str), label_counts.values, color=['skyblue', 'salmon'])
plt.xlabel('Label')
plt.ylabel('Count')
plt.title('Distribution of Labels')
plt.show()


